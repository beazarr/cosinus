<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=<sc>, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>JavaScript</title>
</head>
<body>
<header class="page__header">
        <img src="img/jslogo.png">
        <h1>Nauka programowana w języku JavaScript</h1>
        <nav class="main__nav">
            <ul>
                <li><a href="dbcon.php">bazy danych</a></li>
                <li><a href="js.php">JavaScript</a></li>
                <li><a href="ucz.php">PHP</a></li>
            </ul>
        </nav>
    </header>
    <section class="main__baner">
        <img src="img/javascriptbaner.jpg">
    </section>
<section class="info">
    <h2>Praca na zajęciach w dniu 15.12.2019. Kierunek Informatyka w policealnej szkole Cosinus w Szczecinie</h2>
</section>
    <section class="code">
        <div class="code__box">
        <h3>Prostokąt</h3>

        <form name="formularz" action="">
            <label>Podaj bok a</label><br>
            <input type="text" name="a"><br>
            
            <label>Podaj bok b</label><br>
            <input type="text" name="b">
            <br>
            <input type="button" value="oblicz" onClick="licz(this.form)">
            <br>

            Wynik : <br><br>
            <label>Pole prostokąta:<br></label><input type="text" name="p"><br>
            <label>obwód prostokąta:<br></label><input type="text" name="o">
        </form>
        </div>
        <div class="code__box">
        <h3>Stożek</h3>
        <form name="formularz1" action="">
            <label>podaj promień podstawy</label>
            <input type="text" name="pp">
            <br><br>
            <label>Podaj tworzacą stożka </label>
            <input type="text" name="ts">
            <br>
            <label>Podaj wysokość stożka </label>
            <input type="text" name="hs">
            <br>
            <input type="button" value="oblicz" onClick="oblicz(this.form)">
            <br>

            Wynik:
            <label>pole stożka:<br></label><input type="text" name="ps"><br>
            <label>objętosć stożka:<br></label><input type="text" name="os"><br>

        </form>
        </div>
        <div class="code__box">
        <h3>Kula</h3>
        <form name="formularz2" action="">
            <label>podaj promień kuli</label><br>
            <input type="text" name="promienKuli">
            <br><br>
            <input type="button" value="oblicz" onClick="kula(this.form)">
            <br>

            Wynik:
            <label>pole kuli:<br></label><input type="text" name="poleKuli"><br>
            
        </form>
        </div>
    </section>
        <script src="script.js"></script>
</body>
</html>