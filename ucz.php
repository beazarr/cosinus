<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header class="page__header">
        <img src="img/PHP-logo.svg">
        <h1>Nauka programowana w języku PHP</h1>
        <nav class="main__nav">
            <ul>
                <li><a href="dbcon.php">bazy danych</a></li>
                <li><a href="js.php">JavaScript</a></li>
                <li><a href="ucz.php">PHP</a></li>
            </ul>
        </nav>
    </header>
    <section class="main__baner">
        <img src="img/baner.jpg">
    </section>
    <section class="info">
        <h2>Praca na zajęciach w dniu 15.12.2019. Kierunek Informatyka w policealnej szkole Cosinus w Szczecinie</h2>
    </section>
    <section class="code">
        <div class="code__box">
            <h3> 1 Sprawdzenie poprawności dziąłania kodu w php</h3>
            <?php
            echo ("hello");
            $dzielnik = 2;
            $dzielna = 8;
            @print(" dziala " . $dzielna / $dzielnik);
            ?>
        </div>
        <!--drugi kod -->
        <div class="code__box">
            <h3>2 Drugie dziąłanie kodu php</h3>
            <?php
            $x = 2;
            $y = 3;
            $z = 4;
            $x++;
            $y--;
            $z += $y;
            --$z;
            $x *= $z;
            print(" kolejne dzialanie na php " . $x . " " . $y . " " . $z);
            ?>
        </div>
        <div class="code__box">
            <h3>3 trzeci kod PHP </h3>
            <?php
            $a = 3;
            $b = 5;
            $c = 5;
            if ($a == $b && $b == $c) {
                print("Trójkat o bokach " . $a . " ," . $b . "," . $c . " jest równoboczny");
            } elseif ($a == $b || $b == $c || $a == $c) {
                print("Trójkat o bokach " . $a . " ," . $b . "," . $c . " jest równoramienny");
            } else {
                print("Trójkat o bokach " . $a . " ," . $b . "," . $c . " jest różnoboczny");
            }
            ?>
        </div>
        <div class="code__box">
            <h3>4 Czwarty kod PHP </h3>
            <p>Sprawdzanie czy kolor sygnalizacji świetlnej</p>
            <form action="ucz.php" method="POST" id="forms">
                <input type="text" name="color" placeholder="wpisz liczbę" value="0"><br>
                <input type="submit" name="submit" value="sprawdź!">
            </form>
            <?php
            $kolor =$_POST["color"];
            switch ($kolor) {
                case "czerwony":
                    print("kolor <font color=\"red\"> czerwony STOP!</font>");
                    break;
                case "zielony":
                    print("kolor <font color=\"green\"> zielony IDŹ</font>");
                    break;
                default:
                    print("Podano błędny kolor");
            }
            ?>

        </div>
        <div class="code__box">
            <h3> 5 Piąty kod w PHP </h3>
            <p>Sprawdzanie czy podana liczba jest podzielna przez 2,3,5</p>
            <form action="ucz.php" method="POST" id="forms">
                <input type="text" name="number" placeholder="wpisz liczbę" value="0"><br>
                <input type="submit" name="submit" value="sprawdź!">
            </form>
            <?php
            $numbers = $_POST["number"];

            if ($numbers % 5 == 0) {
                print "liczba " . $numbers . " jest podzielna przez 5";
                print "<br>";
            }
            if ($numbers % 5 != 0) {
                print "liczba " . $numbers . "  nie jest podzielna przez 5";
                print "<br>";
            }
            if ($numbers % 3 == 0) {
                print "liczba " . $numbers . " jest podzielna przez 3";
                print "<br>";
            }

            if ($numbers % 3 != 0) {
                print "liczba " . $numbers . "  nie jest podzielna przez 3";
                print "<br>";
            }
            if ($numbers % 2 == 0) {
                print "liczba " . $numbers . " jest podzielna przez 2";
                print "<br>";
            }

            if ($numbers % 2 != 0) {
                print "liczba " . $numbers . "  nie jest podzielna przez 2";
                print "<br>";
            }
            ?>
        </div>
        <div class="code__box">
            <h3> 6 Przykład uzycia kodu PHP </h3>
            <?php
            $x = 1;
            while (rand(1, 100) != 21) {
                $x++;
            }
            print("<b>21</b> wylosowano za $x razem!");
            ?>

        </div>

        <div class="code__box">
            <h3> 7 Przykład uzycia kodu PHP </h3>
            <?php
            $suma = 0;
            do {
                $x = rand(1, 20);
                $suma = $suma + $x;
                print($x . "+");
            } while ($suma < 100);
            print("0 = " . $suma);
            ?>

        </div>
        <div class="code__box">
            <h3> 8 Przykład uzycia kodu PHP </h3>
            <?php
            $height = 1;
            for ($i = 0; $i <= $height; $i++) {
                for ($j = 0; $j <= $height; $j++) {
                    echo '*';
                }
                echo '<br />';
            }

            ?>

        </div>

    </section>
</body>

</html>